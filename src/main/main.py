#!/usr/bin/env python3

"""Archive Nivose data from Meteo-Ciel"""

import http.client as chttp
import datetime as dt
import os
import sys
import configparser
import mariadb

from bs4 import BeautifulSoup


def parse(data, conn, date, hour):
    """Parse data"""

    cursor = conn.cursor()

    graphs = BeautifulSoup(data, "html.parser").find("span", {"id": "graphs"})

    table = {'0': "temperature", '1': "pressure", '2': 'wind', '3': "humidex", '4': "humidity", '5': "windchill", '114': "snow"}

    for item in graphs.find_all("td"):
        img = item.find("img")["src"]
        ttype = img.split("type=")[1].split("&")[0]

        # Check if timestamp exist
        sql = f"SELECT value FROM {table[ttype]} WHERE YEAR(date) = %d AND MONTH(date) = %d AND DAY(date) = %d AND HOUR(date) = %d"
        val = (date[0], date[1], date[2], hour)

        cursor.execute(sql, val)
        result = cursor.fetchall()

        if not result or result[0][0] is None:
            if "data" in img:
                value = []

                try:
                    value.append(img.split("data" + str(hour) + "=")[1].split("&")[0])
                except IndexError:
                    value.append(None)

                mdate = dt.datetime(date[0], date[1], date[2], hour, 0, 0)

                if ttype in ('0', '1', '3', '4', '5', '114'):
                    if not result:
                        sql = f"INSERT INTO {table[ttype]} (date, value) VALUES (%s, %s)"
                        val = (mdate.strftime('%Y-%m-%d %H:%M:%S'), value[0])
                    else:
                        sql = f"UPDATE {table[ttype]} SET value = %s WHERE date = %s"
                        val = (value[0], mdate.strftime('%Y-%m-%d %H:%M:%S'))

                elif ttype == '2':
                    try:
                        value.append(img.split("supp" + str(hour) + "=")[1].split("&")[0])
                    except IndexError:
                        value.append(None)

                    try:
                        value.append(img.split("angl" + str(hour) + "=")[1].split("&")[0])
                        value[2] = str(int(value[2]) * 10)
                    except IndexError:
                        value.append(None)

                    if not result:
                        sql = "INSERT INTO wind (date, value, gust, direction) VALUES (%s, %s, %s, %s)"
                        val = (mdate.strftime('%Y-%m-%d %H:%M:%S'), value[0], value[1], value[2])
                    else:
                        sql = "UPDATE wind SET value = %s, gust = %s, direction = %s WHERE date = %s"
                        val = (value[0], value[1], value[2], mdate.strftime('%Y-%m-%d %H:%M:%S'))

                cursor.execute(sql, val)
                conn.commit()

    cursor.close()


def leapyear(year):
    """Leap year
       Arguments:
         year : year to test
       Return: True if leap year, else false
    """
    return year % 4 == 0 and year % 100 != 0 or year % 400 == 0


def main():
    """Main function"""

    config = configparser.ConfigParser()
    config.read(os.path.dirname(sys.argv[0]) + '/config.ini')

    try:
        conn = mariadb.connect(
            host=config['database']['host'],
            user=config['database']['user'],
            password=config['database']['password'],
            database=config['database']['database']
        )

    except mariadb.Error as error:
        print(f"Error connecting to MariaDB Platform: {error}")
        sys.exit(1)

    station = config['station']['id']

    if len(sys.argv) > 1 and sys.argv[1] == '--full':
        tdate = []

        cursor = conn.cursor()

        cursor.execute("SELECT date FROM temperature ORDER BY date ASC")
        result = cursor.fetchall()

        for res in result:
            tdate.append(res[0])

        cursor.close()

        timestamp = 1567296000 # 2019-09-01
        now = int(dt.datetime.now().timestamp())
        for i in range(timestamp, now, 3600):
            mdate = dt.datetime.fromtimestamp(i)

            # Recovery data
            if mdate not in tdate:
                print(f'Récupération de {mdate}')

                params = f"code2={station}.&jour2={mdate.day}.&mois2={mdate.month - 1}.&annee2={mdate.year}."
                name = config['server']['pathname'] + "?" + params

                cnx = chttp.HTTPSConnection(config['server']['hostname'])
                cnx.request("GET", name)

                response = cnx.getresponse()
                data = response.read()

                if response.status == 200 and response.reason == 'OK':
                    parse(data, conn, [int(mdate.year), int(mdate.month), int(mdate.day)], mdate.hour)

                cnx.close()

    else:
        cdate = dt.datetime.today().strftime('%Y%m%d')
        hour = dt.datetime.now().hour

        params = f"code2={station}.&jour2={cdate[6:]}.&mois2={int(cdate[4:6]) - 1}.&annee2={cdate[:4]}."
        name = config['server']['pathname'] + "?" + params

        cnx = chttp.HTTPSConnection(config['server']['hostname'])
        cnx.request("GET", name)

        response = cnx.getresponse()
        data = response.read()

        if response.status == 200 and response.reason == 'OK':
            parse(data, conn, [int(cdate[:4]), int(cdate[4:6]), int(cdate[6:])], hour)

        cnx.close()

    conn.close()


if __name__ == "__main__":
    main()
